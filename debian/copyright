Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: cvmfs
Upstream-Contact: CernVM Administrator (cvmadmin) <cernvm.administrator@cern.ch>
Source: https://github.com/cvmfs/cvmfs

Files: *
Copyright: Copyright 2009 CERN
License: BSD-3-Clause
 Copyright (c) CERN
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 .
    * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
    * Neither the name of CERN nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
Files: debian/*
Copyright: 2022 Yachen Wang <oripoin@outlook.com>
License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

Files: externals/vjson/src/*
Copyright: 2010, Ivan Vashchaev <vivkin@gmail.com>
License: MIT
 Copyright 2010 Ivan Vashchaev <vivkin@gmail.com>
 .
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Files: externals/sha3/src/*
Copyright: Keccak Code Package
License: CC0-1.0
 The person who associated a work with this deed has dedicated the work to the public domain by waiving all of his or her rights to the work worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 .
 You can copy, modify, distribute and perform the work, even for commercial purposes, all without asking permission. See Other Information below.

Files: externals/sha3/src/brg_endian.h
Copyright: Brian Gladman
License: BSD-3-clause
 Copyright (c) 1998-2008, Brian Gladman, Worcester, UK. All rights reserved.
 .
 LICENSE TERMS
 .
 The redistribution and use of this software (with or without changes)
 is allowed without the payment of fees or royalties provided that:
 .
  1. source code distributions include the above copyright notice, this
     list of conditions and the following disclaimer;
 .
  2. binary distributions include the above copyright notice, this list
     of conditions and the following disclaimer in their documentation;
 .
  3. the name of the copyright holder is not used to endorse products
     built using this software without specific written permission.
 .
 DISCLAIMER
 .
 This software is provided 'as is' with no explicit or implied warranties
 in respect of its properties, including, but not limited to, correctness
 and/or fitness for purpose.

Files: externals/sha2/src/*
Copyright: 2006-2015, ARM Limited
License: Apache-2.0
 Copyright 2006-1015 ARM Limited
 .
 Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 .
    http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
